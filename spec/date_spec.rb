require 'spec_helper'

describe Date do
  let(:date) { Date.parse('1957-09-22') }

  it 'expands to its day' do
    expect(date.day_containing).to eq(Period.new('1957-09-22', '1957-09-22'))
  end

  it 'expands to its week' do
    ::Date.beginning_of_week = :sunday
    expect(date.week_containing).to eq(Period.new('1957-09-22', '1957-09-28'))
  end

  it 'expands to its biweek' do
    ::Date.beginning_of_week = :sunday
    expect(date.biweek_containing).to eq(Period.new('1957-09-15', '1957-09-28'))
  end

  it 'expands to its semimonth' do
    expect(date.semimonth_containing).to eq(Period.new('1957-09-16', '1957-09-30'))
  end

  it 'expands to its month' do
    expect(date.month_containing).to eq(Period.new('1957-09-01', '1957-09-30'))
  end

  it 'expands to its bimonth' do
    expect(date.bimonth_containing).to eq(Period.new('1957-09-01', '1957-10-31'))
  end

  it 'expands to its quarter' do
    expect(date.quarter_containing).to eq(Period.new('1957-07-01', '1957-09-30'))
  end

  it 'expands to its half' do
    expect(date.half_containing).to eq(Period.new('1957-07-01', '1957-12-31'))
  end

  it 'expands to its year' do
    expect(date.year_containing).to eq(Period.new('1957-01-01', '1957-12-31'))
  end

  it 'expands to chunk periods' do
    ::Date.beginning_of_week = :monday
    expect(Date.parse('2013-07-04').expand_to_period(:year))
      .to eq Period.new('2013-01-01', '2013-12-31')
    expect(Date.parse('2013-07-04').expand_to_period(:half))
      .to eq Period.new('2013-07-01', '2013-12-31')
    expect(Date.parse('2013-07-04').expand_to_period(:quarter))
      .to eq Period.new('2013-07-01', '2013-09-30')
    expect(Date.parse('2013-07-04').expand_to_period(:bimonth))
      .to eq Period.new('2013-07-01', '2013-08-31')
    expect(Date.parse('2013-07-04').expand_to_period(:month))
      .to eq Period.new('2013-07-01', '2013-07-31')
    expect(Date.parse('2013-07-04').expand_to_period(:semimonth))
      .to eq Period.new('2013-07-01', '2013-07-15')
    expect(Date.parse('2013-07-04').expand_to_period(:biweek))
      .to eq Period.new('2013-07-01', '2013-07-14')
    expect(Date.parse('2013-07-04').expand_to_period(:week))
      .to eq Period.new('2013-07-01', '2013-07-07')
    expect(Date.parse('2013-07-04').expand_to_period(:day))
      .to eq Period.new('2013-07-04', '2013-07-04')
  end
end
