require 'date'

module FatPeriod
  # An extension of Date for methods useful with respect to FatPeriod::Periods.
  module Date
    # Return the Period of the given chunk size that contains this Date. Chunk
    # can be one of :year, :half, :quarter, :bimonth, :month, :semimonth,
    # :biweek, :week, or :day.
    #
    # @example
    #   date = Date.parse('2015-06-13')
    #   date.expand_to_period(:week)      #=> Period(2015-06-08..2015-06-14)
    #   date.expand_to_period(:semimonth) #=> Period(2015-06-01..2015-06-15)
    #   date.expand_to_period(:quarter)   #=> Period(2015-04-01..2015-06-30)
    #
    # @param chunk [Symbol] one of :year, :half, :quarter, :bimonth, :month,
    #    :semimonth, :biweek, :week, or :day
    # @return [Period] Period of size `chunk` containing self
    def expand_to_period(chunk)
      require 'fat_period'
      Period.new(beginning_of_chunk(chunk), end_of_chunk(chunk))
    end

    # Return a period representing a chunk containing a given Date.
    def day_containing
      Period.new(self, self)
    end

    def week_containing
      Period.new(self.beginning_of_week, self.end_of_week)
    end

    def biweek_containing
      Period.new(self.beginning_of_biweek, self.end_of_biweek)
    end

    def semimonth_containing
      Period.new(self.beginning_of_semimonth, self.end_of_semimonth)
    end

    def month_containing
      Period.new(self.beginning_of_month, self.end_of_month)
    end

    def bimonth_containing
      Period.new(self.beginning_of_bimonth, self.end_of_bimonth)
    end

    def quarter_containing
      Period.new(self.beginning_of_quarter, self.end_of_quarter)
    end

    def half_containing
      Period.new(self.beginning_of_half, self.end_of_half)
    end

    def year_containing
      Period.new(self.beginning_of_year, self.end_of_year)
    end

    def chunk_containing(chunk)
      raise ArgumentError, 'chunk is nil' unless chunk

      chunk = chunk.to_sym
      raise ArgumentError, "unknown chunk name: #{chunk}" unless CHUNKS.include?(chunk)

      method = "#{chunk}_containing".to_sym
      send(method, self)
    end
  end
end

# An extension Date for methods useful with respect to FatPeriod::Periods.
class Date
  include FatPeriod::Date
  # @!parse include FatPeriod::Date
  # @!parse extend FatPeriod::Date::ClassMethods
end
